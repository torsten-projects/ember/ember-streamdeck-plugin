# Buttons
- single button
  - shows current temperature with colored background by state - off, stabilized (at temperature), cooling or heating
  - on press
    - if target temperature is 0
      - set target temperature to 57
    - if target temperature is 1+
      - set target temperature to 0
